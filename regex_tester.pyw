# -*- coding:utf-8 -*-
import wx
import re


class RegexWindow(wx.Frame):
    
    def __init__(self, parent, title, wsize):
        wx.Frame.__init__(self, parent, title=title, size=wsize)
        
        self.SetBackgroundColour("#FFFFFF")
        self.SetMinSize(self.GetSize())
        # font_mono = wx.Font(10,
        #                     wx.FONTFAMILY_DEFAULT,
        #                     wx.FONTSTYLE_NORMAL,
        #                     wx.FONTWEIGHT_NORMAL,
        #                     faceName="Courier New")

        self.init_menu()

        self._font_attr = wx.TextAttr()
        self._font_attr.SetTextColour("#FF0000")
        self._font_attr.SetBackgroundColour("#FFFF00")

        ####
        # "main" controls
        self._pa0 = wx.Panel(self)

        ####
        # "search text" controls
        self._bx01 = wx.BoxSizer(wx.VERTICAL)
        self._bx01_sbox = wx.StaticBox(self._pa0, -1, label="Search Text")
        self._te_src = wx.TextCtrl(self._pa0, -1, style=wx.TE_MULTILINE | wx.HSCROLL | wx.TE_RICH2)
        self._te_src.SetMaxLength(0)

        # boxsizer
        self._bx011 = wx.StaticBoxSizer(self._bx01_sbox, wx.VERTICAL)
        self._bx011.Add(self._te_src, proportion=1, flag=wx.EXPAND | wx.ALL, border=5)
        self._bx01.Add(self._bx011, proportion=1, flag=wx.EXPAND, border=0)
        
        ####
        # "regular expression" controls
        self._bx02 = wx.BoxSizer(wx.VERTICAL)
        self._bx02_sbox = wx.StaticBox(self._pa0, -1, label="Regular Expression")

        self._te_regex_search = wx.TextCtrl(self._pa0, -1, style=wx.TE_MULTILINE, size=(-1, 100))
        self._te_regex_search.SetBackgroundColour("#EDEDED")
        self._btn_regex_search = wx.Button(self._pa0, label="Search", size=(-1, 30))

        self._te_regex_replace = wx.TextCtrl(self._pa0, -1, style=wx.TE_MULTILINE, size=(-1, 100))
        self._te_regex_replace.SetBackgroundColour("#EDEDED")
        self._btn_regex_replace = wx.Button(self._pa0, label="Replace", size=(-1, 30))

        self._wc021_label = wx.StaticText(self._pa0, -1, "[FLAG]")
        self._cb1 = wx.CheckBox(self._pa0, -1, "DOTALL")
        self._cb2 = wx.CheckBox(self._pa0, -1, "IGNORECASE")
        self._cb3 = wx.CheckBox(self._pa0, -1, "LOCALE")
        self._cb4 = wx.CheckBox(self._pa0, -1, "MULTILINE")
        self._cb5 = wx.CheckBox(self._pa0, -1, "VERBOSE")
        self._cb6 = wx.CheckBox(self._pa0, -1, "UNICODE")
        self._cb7 = wx.CheckBox(self._pa0, -1, "REPLACE FROM SEARCH FIELD")

        # regex search boxsizer
        self._bx02111 = wx.BoxSizer(wx.VERTICAL)
        self._bx02111.Add(wx.StaticText(self._pa0, label=r'**backreferences** (\w+)\s+\1 or (?P<name>\w+)\s+(?P=name)'), proportion=0, flag=wx.EXPAND, border=0)
        self._bx02111.Add(self._te_regex_search, proportion=0, flag=wx.EXPAND, border=0)
        self._bx02111.Add(self._btn_regex_search, proportion=0, flag=wx.EXPAND, border=0)

        # regex replace boxsizer
        self._bx02112 = wx.BoxSizer(wx.VERTICAL)
        self._bx02112.Add(wx.StaticText(self._pa0, label=r'**backreferences** \2 or \g<2> or \g<name>'), proportion=0, flag=wx.EXPAND, border=0)
        self._bx02112.Add(self._te_regex_replace, proportion=0, flag=wx.EXPAND, border=0)
        self._bx02112.Add(self._btn_regex_replace, proportion=0, flag=wx.EXPAND, border=0)

        # regex search + replace boxsizer
        self._bx0211 = wx.BoxSizer(wx.HORIZONTAL)
        self._bx0211.Add(self._bx02111, proportion=1, flag=wx.EXPAND, border=0)
        self._bx0211.Add(self._bx02112, proportion=1, flag=wx.EXPAND, border=0)

        # regex label boxsizer
        self._bx0212 = wx.BoxSizer(wx.HORIZONTAL)
        self._bx0212.Add(self._wc021_label, proportion=0, flag=wx.RIGHT, border=10)
        self._bx0212.Add(self._cb1, proportion=0, flag=0, border=0)
        self._bx0212.Add(self._cb2, proportion=0, flag=0, border=0)
        self._bx0212.Add(self._cb3, proportion=0, flag=0, border=0)
        self._bx0212.Add(self._cb4, proportion=0, flag=0, border=0)
        self._bx0212.Add(self._cb5, proportion=0, flag=0, border=0)
        self._bx0212.Add(self._cb6, proportion=0, flag=0, border=0)
        self._bx0212.Add(self._cb7, proportion=0, flag=0, border=0)

        # boxsizer
        self._bx021 = wx.StaticBoxSizer(self._bx02_sbox, wx.VERTICAL)
        self._bx021.Add(self._bx0211, proportion=0, flag=wx.EXPAND | wx.ALL, border=10)
        self._bx021.Add(self._bx0212, proportion=0, flag=wx.EXPAND | wx.ALL, border=10)
        self._bx02.Add(self._bx021, proportion=0, flag=wx.EXPAND, border=0)
        
        ####
        # "reseult" controls
        self._bx03 = wx.BoxSizer(wx.VERTICAL)
        self._bx03_sbox = wx.StaticBox(self._pa0, -1, label="Result")
        self._wc031_nb = wx.Notebook(self._pa0, -1, style=wx.NB_TOP)
        
        # tab 1
        self._pa0311 = wx.Panel(self._wc031_nb)
        self._te_result = wx.TextCtrl(self._pa0311, -1,
                                      style=wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2)
        self._te_result.SetMaxLength(0)
        self._bx0311 = wx.BoxSizer(wx.VERTICAL)
        self._bx0311.Add(self._te_result, proportion=1, flag=wx.EXPAND, border=0)
        self._pa0311.SetSizer(self._bx0311)
        
        # tab 2
        self._pa0312 = wx.Panel(self._wc031_nb)
        self._listctrl = wx.ListCtrl(self._pa0312, -1, style=wx.LC_REPORT)
        self._wc0312_label = wx.StaticText(self._pa0312, -1, label="Count : 0")
        self._bx0312 = wx.BoxSizer(wx.VERTICAL)
        self._bx0312.Add(self._listctrl, proportion=1, flag=wx.EXPAND, border=0)
        self._bx0312.Add(self._wc0312_label, proportion=0, flag=0, border=0)
        self._pa0312.SetSizer(self._bx0312)

        # tab 3
        self._pa0313 = wx.Panel(self._wc031_nb)
        self._te_values = wx.TextCtrl(self._pa0313, -1, style=wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL)
        self._te_values.SetMaxLength(0)
        self._bx0313 = wx.BoxSizer(wx.VERTICAL)
        self._bx0313.Add(self._te_values, proportion=1, flag=wx.EXPAND, border=0)
        self._pa0313.SetSizer(self._bx0313)

        # tab 4
        self._pa0314 = wx.Panel(self._wc031_nb)
        self._te_replaced = wx.TextCtrl(self._pa0314, -1, style=wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL)
        self._te_replaced.SetMaxLength(0)
        self._bx0314 = wx.BoxSizer(wx.VERTICAL)
        self._bx0314.Add(self._te_replaced, proportion=1, flag=wx.EXPAND)
        self._pa0314.SetSizer(self._bx0314)

        # add notebook page
        self._wc031_nb.AddPage(self._pa0311, "Result")
        self._wc031_nb.AddPage(self._pa0312, "List")
        self._wc031_nb.AddPage(self._pa0313, "Values")
        self._wc031_nb.AddPage(self._pa0314, "Replace")
        
        # sizer
        self._bx031 = wx.StaticBoxSizer(self._bx03_sbox, wx.VERTICAL)
        self._bx031.Add(self._wc031_nb, proportion=1, flag=wx.EXPAND | wx.ALL, border=5)
        self._bx03.Add(self._bx031, proportion=1, flag=wx.EXPAND, border=0)

        ####
        # main boxsizer
        self._bx0 = wx.BoxSizer(wx.VERTICAL)
        self._bx0.Add(self._bx01, proportion=1, flag=wx.EXPAND | wx.ALL, border=10)
        self._bx0.Add(self._bx02, proportion=0, flag=wx.EXPAND | wx.ALL, border=10)
        self._bx0.Add(self._bx03, proportion=1, flag=wx.EXPAND | wx.ALL, border=10)
        self._pa0.SetSizer(self._bx0)

        ####
        # bind events
        self._btn_regex_search.Bind(wx.EVT_BUTTON, self.regex_search)
        self._btn_regex_replace.Bind(wx.EVT_BUTTON, self.regex_replace)

        ####
        # finalize
        self.Show()
        self.Center()

    def regex_search(self, e):
        te_regex = self._te_regex_search
        te_src, te_result, te_values,  = self._te_src, self._te_result, self._te_values
        res_list, res_count = self._listctrl, self._wc0312_label
        
        search_regex = te_regex.GetValue()
        if not search_regex:
            return

        # initialization
        te_result.Clear()
        te_values.Clear()
        
        res_list.DeleteAllItems()
        res_list.DeleteAllColumns()
        res_list.InsertColumn(0, "Position")
        res_list.InsertColumn(1, "Value")

        src_text = te_src.GetValue()
        te_result.ChangeValue(src_text)
        
        # regex routin
        flag = 0
        if self._cb1.IsChecked():
            flag |= re.DOTALL
        if self._cb2.IsChecked():
            flag |= re.IGNORECASE
        if self._cb3.IsChecked():
            flag |= re.LOCALE
        if self._cb4.IsChecked():
            flag |= re.MULTILINE
        if self._cb5.IsChecked():
            flag |= re.VERBOSE
        if self._cb6.IsChecked():
            flag |= re.UNICODE
        
        p = re.compile(search_regex, flag)
        
        num_of_groups = p.groups
        d_named_group = p.groupindex
        
        for x in xrange(num_of_groups):
            gname = ""
            for key in d_named_group.keys():
                if d_named_group[key] == (x+1):
                    gname = key
                    break
            
            cname = "\\"+str(x+1)
            if gname:
                cname += "(=\\g<"+gname+">)"
            
            res_list.InsertColumn(res_list.GetColumnCount(), cname)

        for m in p.finditer(src_text):
            
            # tab 1
            te_result.SetStyle(m.start(), m.end(), self._font_attr)
            
            # tab 2
            value = '('+str(m.start())+', '+str(m.end())+')'
            index = res_list.InsertStringItem(res_list.GetItemCount(), label=value)
            res_list.SetStringItem(index, 1, label=m.group())
            if num_of_groups:
                for x in xrange(num_of_groups):
                    if m.group(x+1):
                        res_list.SetStringItem(index, x+2, label=m.group(x+1))
            
            # tab 3
            te_values.AppendText(m.group()+'\n')
        
        # SetColumnWidth    
        # for x in xrange(res_list.GetColumnCount()):
            # res_list.SetColumnWidth(x, -2)
        
        # GetItemCount
        res_count.SetLabel("Count : " + str(res_list.GetItemCount()))

    def regex_replace(self, e):
        regex = self._te_regex_replace.GetValue()
        if not regex:
            return

        flag = 0
        if self._cb1.IsChecked():
            flag |= re.DOTALL
        if self._cb2.IsChecked():
            flag |= re.IGNORECASE
        if self._cb3.IsChecked():
            flag |= re.LOCALE
        if self._cb4.IsChecked():
            flag |= re.MULTILINE
        if self._cb5.IsChecked():
            flag |= re.VERBOSE
        if self._cb6.IsChecked():
            flag |= re.UNICODE

        _te_src = self._te_src if self._cb7.IsChecked() else self._te_values
        p = re.compile(self._te_regex_search.GetValue(), flag)
        self._te_replaced.SetValue(p.sub(regex, _te_src.GetValue()))
        self._wc031_nb.ChangeSelection(3)
        
    def init_menu(self):
        self.CreateStatusBar()
        self.SetStatusText("This is the statusbar")

        menu_bar = wx.MenuBar()
        
        menu1 = wx.Menu()
        menu1.AppendSeparator()
        menu_exit = menu1.Append(wx.ID_EXIT, "&Exit\tCtrl+X", "Exit")
        
        menu2 = wx.Menu()
        menu2.AppendSeparator()
        menu_select_all = menu2.Append(wx.ID_ANY, "Select &All\tCtrl+A", "Select All")

        menu3 = wx.Menu()
        menu3.AppendSeparator()
        menu_about = menu3.Append(wx.ID_ABOUT, "&About", "About this program")
        
        menu_bar.Append(menu1, "&File")
        menu_bar.Append(menu2, "&Edit")
        menu_bar.Append(menu3, "&About")
 
        self.Bind(wx.EVT_MENU, self.onExit, menu_exit)
        self.Bind(wx.EVT_MENU, self.onSelectAll, menu_select_all)
        self.Bind(wx.EVT_MENU, self.onAbout, menu_about)
        
        self.SetMenuBar(menu_bar)
        
    def onExit(self, e):
        self.Close(True)
        
    def onSelectAll(self, e):
        te = self.FindFocus()  # wx.Frame.FindFocus()
        if isinstance(te, wx.TextCtrl):
            te.SetSelection(-1, -1)
            
    def onAbout(self, e):
        dlg = wx.MessageDialog(self, 'Regular Expresstion Tester\nv1.0.0', 'About', wx.OK)
        dlg.ShowModal()
        dlg.Destroy()

        
####
# tester
if __name__ == '__main__':
    app = wx.App(False)
    window = RegexWindow(None, 'Regular Expression Tester', (800, 800))
    app.MainLoop()
